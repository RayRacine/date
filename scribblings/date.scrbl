#lang scribble/manual
@require[@for-label[date
                    racket/base]]

@title{date}
@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]

@section{Date}
@defmodule[date]

@defidform[#:kind "Type" Date]{
Opaque datetype representing an exact date/time moment.
}

@defidform[#:kind "Type" Time]{
Opaque datatype of a time value.
}

@defthing[#:kind "Procedure" current-date-string-iso-8601 (-> String)]{
The current date/time as an ISO-8601 formatted string.
}

@defthing[#:kind "Procedure" iso-8601-date-string->date (-> String Date)]{
Parse a ISO-8601 formatted string into an Date value.
}


@defthing[#:kind "Procedure" current-date-string-rfc-2822 (-> String)]{
The current date/time as an RFC-2822 formatted string.
}

@defproc[(current-date [tz-offset Integer]) Date]{
Returns the Date for the current UTC time for the given TZ-OFFSET integer value.
Typed wrapper around SRFI/19's current-date.
}

@defproc[(local-current-date) Data]{
Returns the Date for the current local TZ.
}

@defproc[(date->time-utc [d Date]) Time]{
Returns the time in UTC for the given date.
}

@defproc[(current-time) Time]{
Returns the current time in UTC.
}

@defproc[(time= [t1 Time] [t2 Time]) Boolean] {
Time equality function.
}

@defproc[(time< [t1 Time] [t2 Time]) Boolean] {
Time comparison function.
}

@defproc[(time<= [t1 Time] [t2 Time]) Boolean] {
Time comparison function.
}

@defproc[(time> [t1 Time] [t2 Time]) Boolean] {
Time comparison function.
}

@defproc[(time>= [t1 Time] [t2 Time]) Boolean] {
Time comparison function.
}

@defproc[(local-zone-offset) Integer]{
Date time zone offset for the local time zone.
}