date
====
Very small Date/Time library that currently is directed towards support for commonly encountered Date/Time formats on the Web.  Specifically RFC2822 and ISO8601.

Currently there are a number of richer, general purpose Date/Time packages available on Racket's package system.
In other words, there are probably better options than this collection unless you just need an RFC/ISO data for some HTP API or similar right now.

Currently this package sits over SRFI/19, Scheme's Date/Time library.  Ideally, this package will one day re-plateform and become a comprehensive, general purpose library for Typed Racket.

Note:  As this package currently squats on the desirable 'date' collection/package name, more than happy to replace this collection/package if/when someone has a kick-ass replacement ready to go.
